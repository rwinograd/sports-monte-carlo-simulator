package ca.winograd.league;

import java.util.Set;

public interface SeasonStrategy {
	public Set<Team> simulate();
}
