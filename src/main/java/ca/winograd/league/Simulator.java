package ca.winograd.league;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

public class Simulator {
	
	public static class PlayoffTeamChance implements Comparable<PlayoffTeamChance>{
		public final double chance;
		public final Team team;
		
		public PlayoffTeamChance(Team team, double chance) {
			this.team = team;
			this.chance = chance;
		}

		public int compareTo(PlayoffTeamChance o) {
			if(chance != o.chance) {
				return Double.compare(o.chance, chance);
			}
			
			return team.getName().compareTo(o.team.getName());
		}
	}

	private final League league;
	private final SeasonStrategy strategy;
	
	public Simulator(League league, SeasonStrategy strategy) {
		this.strategy = strategy;
		this.league = league;
	}


	public List<PlayoffTeamChance> monteCarlo(int iterations) {
		Map<Team, AtomicInteger> playoffCounts = new HashMap<Team, AtomicInteger>();
		for(Team team : league.getTeams()) {
			playoffCounts.put(team, new AtomicInteger(0));
		}
		
		for(int i = 0; i < iterations; i++) {
			Set<Team> playoffTeams = strategy.simulate();
			for(Team playoffTeam : playoffTeams) {
				playoffCounts.get(playoffTeam).addAndGet(1);
			}
		}
		
		SortedSet<PlayoffTeamChance> chances = new TreeSet<PlayoffTeamChance>();
		for(Map.Entry<Team, AtomicInteger> entry : playoffCounts.entrySet()) {
			chances.add(new PlayoffTeamChance(entry.getKey(), (1.0*entry.getValue().get() / iterations)));
		}
		
		return new LinkedList<PlayoffTeamChance>(chances);
	}

}
