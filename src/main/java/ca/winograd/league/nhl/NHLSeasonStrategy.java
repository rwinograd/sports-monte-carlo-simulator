package ca.winograd.league.nhl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ca.winograd.league.SeasonStrategy;
import ca.winograd.league.Team;

public class NHLSeasonStrategy implements SeasonStrategy {

	private final NHLLeague league;
	private final NHLSchedule schedule;
	private final RankingStrategy strategy = new RankingStrategy();
	private final GameSimulator gameSimulator = new GameSimulator();
	
	public NHLSeasonStrategy(NHLLeague league, NHLSchedule schedule) {
		this.league = league;
		this.schedule = schedule;
	}
	
	public Set<Team> simulate() {
		Map<Team, TeamSeasonResult> results = new HashMap<Team, TeamSeasonResult>();
		for(Team team : league.getTeams()) {
			results.put(team, new TeamSeasonResult(team));
		}
		
		saveExistingGames(results);
		simulateGames(results);
		
		return strategy.getPlayoffTeams(league, results);
	}

	private void saveExistingGames(Map<Team, TeamSeasonResult> results) {
		for(Game.Result result : schedule.getPlayedGames()) {
			results.get(result.getHomeTeam()).accumulateHome(result);
			results.get(result.getAwayTeam()).accumulateAway(result);
		}
	}

	private void simulateGames(Map<Team, TeamSeasonResult> results) {
		for(Game game : schedule.getUnplayedGames()) {
			Game.Result result = gameSimulator.simulate(game);
			results.get(game.getHomeTeam()).accumulateHome(result);
			results.get(game.getAwayTeam()).accumulateAway(result);
		}
	}
}
