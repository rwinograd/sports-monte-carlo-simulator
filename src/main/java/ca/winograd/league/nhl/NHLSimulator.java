package ca.winograd.league.nhl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.winograd.league.Simulator;
import ca.winograd.league.Simulator.PlayoffTeamChance;
import ca.winograd.league.Team;
import ca.winograd.league.nhl.NHLLeague.Conference;
import ca.winograd.league.nhl.NHLLeague.Division;

public class NHLSimulator {

	private final NHLLeague league;
	
	public NHLSimulator() {
		Conference west = new Conference("Western", 
			new Division("Pacific",
				new Team("Los Angeles"), new Team("Anaheim"), new Team("San Jose"), new Team("Vancouver"), 
				new Team("Edmonton"), new Team("Calgary"), new Team("Arizona")), 
			new Division("Central",
				new Team("Winnipeg"), new Team("Minnesota"), new Team("Colorado"), new Team("Dallas"),  
				new Team("St. Louis"), new Team("Nashville"),new Team("Chicago"))
		);
		Conference east = new Conference("Eastern", 
			new Division("Metropolitan",
				new Team("NY Rangers"), new Team("NY Islanders"), new Team("Washington"), new Team("New Jersey"), 
				new Team("Pittsburgh"), new Team("Philadelphia"), new Team("Carolina"), new Team("Columbus")), 
			new Division("Atlantic",
				new Team("Montréal"), new Team("Detroit"), new Team("Toronto"), new Team("Boston"), 
				new Team("Ottawa"), new Team("Florida"), new Team("Tampa Bay"), new Team("Buffalo"))
		);		
		this.league = new NHLLeague(west, east);
	}

	private static Map<String, Team> flattenToMap(List<Team> teams) {
		Map<String, Team> teamMap = new HashMap<String, Team>();
		for(Team team : teams) {
			teamMap.put(team.getName(), team);
		}
		
		return teamMap;
	}

	public List<PlayoffTeamChance> getPlayoffChances() throws IOException {
		NHLSchedule schedule = NHLSchedule.newInstance(flattenToMap(league.getTeams()));
		
		Simulator simulation = new Simulator(league, new NHLSeasonStrategy(league, schedule));
		List<PlayoffTeamChance> playoffProbabilities = simulation.monteCarlo(10000);
		
		for(PlayoffTeamChance teamChances : playoffProbabilities) {
			System.out.println(String.format("%s: %.2f%%", teamChances.team, 100*teamChances.chance));
		}
		
		return playoffProbabilities;
	}
}
