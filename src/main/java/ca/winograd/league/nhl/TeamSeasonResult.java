package ca.winograd.league.nhl;

import ca.winograd.league.Team;

public class TeamSeasonResult implements Comparable<TeamSeasonResult> {
	public final Team team;
	
	// Four final ways to end the game
	public int wins;
	public int regulationLosses;
	public int overtimeLosses;
	public int shootoutLosses;

	public int shootoutWins;

	public TeamSeasonResult(Team team) {
		this.team = team;
	}
	
	public void accumulateHome(Game.Result result) {
		if(result.awayScore > result.homeScore) {
			if(result.isRegulation()) {
				regulationLosses++;
			} else if (result.isOvertime()) {
				overtimeLosses++;
			} else {
				shootoutLosses++;
			}
		} else {
			wins++;

			if(result.isShootout()) {
				shootoutWins++;
			}
		}
	}
	
	public void accumulateAway(Game.Result result) {
		if(result.awayScore > result.homeScore) {
			wins++;
			
			if(result.isShootout()) {
				shootoutWins++;
			}
		} else {
			if(result.isRegulation()) {
				regulationLosses++;
			} else if (result.isOvertime()) {
				overtimeLosses++;
			} else {
				shootoutLosses++;
			}
		}
	}

	public int compareTo(TeamSeasonResult o) {
		int points = getPoints();
		int otherPoints = o.getPoints();
		if(points != otherPoints) {
			return (otherPoints - points);
		}
		
		int games = getGames();
		int otherGames = o.getGames();
		if(games != otherGames) {
			return (otherGames - games);
		}
		
		int row = getRow();
		int otherRow = o.getRow();
		if(row != otherRow) {
			return (otherRow - row);
		}
		
		return team.getName().compareTo(o.team.getName());
		
		// TODO:
		// 3. The greater number of points earned in games between the tied clubs. 
		// If two clubs are tied, and have not played an equal number of home games against each other,
		// points earned in the first game played in the city that had the extra game shall not be included
		// If more than two clubs are tied, the higher percentage of available points earned in games among
		// those clubs, and not including any "odd" games, shall be used to determine the standing.
		
		// 4. The greater differential between goals for and against for the entire regular season.
	}

	private int getRow() {
		return (wins - shootoutWins);
	}

	private int getPoints() {
		return 2*wins + overtimeLosses + shootoutLosses;
	}

	private int getGames() {
		return wins + regulationLosses + overtimeLosses + shootoutLosses;
	}
}
