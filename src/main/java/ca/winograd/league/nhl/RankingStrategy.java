package ca.winograd.league.nhl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import ca.winograd.league.Team;

import ca.winograd.league.nhl.NHLLeague.Conference;
import ca.winograd.league.nhl.NHLLeague.Division;

public class RankingStrategy {

	public Set<Team> getPlayoffTeams(NHLLeague league, Map<Team, TeamSeasonResult> results) {
		Set<Team> teams = new HashSet<Team>();
		
		for(Conference conference : league.getConferences()) {
			List<Team> conferenceResults = getConferenceRanking(conference, results);
			teams.addAll(firstN(conferenceResults, 8));
		}
		
		return teams;
	}

	private <T> Collection<T> firstN(Iterable<T> all, int max) {
		List<T> teams = new LinkedList<T>();
		
		int i = 0;
		final Iterator<T> it = all.iterator();
		while(i < max && it.hasNext()) {
			teams.add(it.next());
			
			i++;
		}
		
		return teams;
	}

	private List<Team> getConferenceRanking(Conference conference, Map<Team, TeamSeasonResult> results) {
		Division division1 = conference.getDivisionA();
		Division division2 = conference.getDivisionB();

		Queue<Team> divisionRanking1 = new LinkedList<Team>(getDivisionRanking(division1, results));
		Queue<Team> divisionRanking2 = new LinkedList<Team>(getDivisionRanking(division2, results));
		
		List<Team> finalRanking = new LinkedList<Team>();
		
		// Top two teams go in in sorted order
		SortedSet<TeamSeasonResult> sorted = new TreeSet<TeamSeasonResult>();
		sorted.add(results.get(divisionRanking1.poll()));
		sorted.add(results.get(divisionRanking2.poll()));
		for(TeamSeasonResult teamSeasonResult : sorted) {
			finalRanking.add(teamSeasonResult.team);
		}
		
		// Next two teams from each division in sorted order
		sorted.clear();
		sorted.add(results.get(divisionRanking1.poll()));
		sorted.add(results.get(divisionRanking1.poll()));
		sorted.add(results.get(divisionRanking2.poll()));
		sorted.add(results.get(divisionRanking2.poll()));
		for(TeamSeasonResult teamSeasonResult : sorted) {
			finalRanking.add(teamSeasonResult.team);
		}
		
		// Top two of remaining teams
		sorted.clear();
		sorted.add(results.get(divisionRanking1.poll()));
		sorted.add(results.get(divisionRanking1.poll()));
		sorted.add(results.get(divisionRanking2.poll()));
		sorted.add(results.get(divisionRanking2.poll()));
		for(TeamSeasonResult teamSeasonResult : firstN(sorted, 2)) {
			finalRanking.add(teamSeasonResult.team);
		}
		
		return finalRanking;
	}
	
	private List<Team> getDivisionRanking(Division division, Map<Team, TeamSeasonResult> results) {
		SortedSet<TeamSeasonResult> sorted = new TreeSet<TeamSeasonResult>();
		for(Team team : division.getTeams()) {
			sorted.add(results.get(team));
		}
		
		List<Team> finalRanking = new LinkedList<Team>();
		for(TeamSeasonResult teamSeasonResult : sorted) {
			finalRanking.add(teamSeasonResult.team);
		}
		
		return finalRanking;
	}

}
