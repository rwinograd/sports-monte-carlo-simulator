package ca.winograd.league.nhl;

import java.util.Random;

import ca.winograd.league.nhl.Game.Result;

public class GameSimulator {

	private final Random random = new Random(System.currentTimeMillis());
	
	public Game.Result simulate(Game game) {
		// For now we use a coin flip to decide on home versus away win
		if(random.nextInt(2) == 1) {
			return homeWinResult(game);
		} else {
			return awayWinResult(game);
		}
	}

	private static Result homeWinResult(Game game) {
		return new Game.Result(game, 1, 0, 3);
	}

	private static Result awayWinResult(Game game) {
		return new Game.Result(game, 0, 1, 3);
	}
}
