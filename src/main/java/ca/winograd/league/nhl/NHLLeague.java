package ca.winograd.league.nhl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ca.winograd.league.League;
import ca.winograd.league.Team;

public class NHLLeague implements League {
	
	public static class Conference {

		private final String name;
		private final Division divisionA;
		private final Division divisionB;

		public final List<Team> allTeams;
		
		public Conference(String name, Division divisionA, Division divisionB) {
			this.name = name;
			this.divisionA = divisionA;
			this.divisionB = divisionB;

			List<Team> allTeams = new LinkedList<Team>();
			allTeams.addAll(divisionA.getTeams());
			allTeams.addAll(divisionB.getTeams());
			
			this.allTeams = Collections.unmodifiableList(allTeams);
		}
		
		public String getName() {
			return name;
		}

		public Division getDivisionA() {
			return divisionA;
		}
		
		public Division getDivisionB() {
			return divisionB;
		}

		public List<Team> getTeams() {
			return allTeams;
		}
	}
	
	public static class Division {

		private final String name;
		private final List<Team> teams;
		
		public Division(String name, Team ... teams) {
			this.name = name;
			
			List<Team> list = new LinkedList<Team>();
			for(Team team : teams) {
				list.add(team);
			}
			
			this.teams = Collections.unmodifiableList(list);
		}
		
		public String getName() {
			return name;
		}

		public List<Team> getTeams() {
			return teams;
		}

	}
	
	public final Conference conferenceA;
	public final Conference conferenceB;

	public final List<Team> allTeams;
	public final List<Conference> allConferences;
	
	public NHLLeague(Conference conferenceA, Conference conferenceB) {
		this.conferenceA = conferenceA;
		this.conferenceB = conferenceB;

		List<Team> allTeams = new LinkedList<Team>();
		allTeams.addAll(conferenceA.getTeams());
		allTeams.addAll(conferenceB.getTeams());
		
		this.allTeams = Collections.unmodifiableList(allTeams);

		List<Conference> allConferences = new LinkedList<Conference>();
		allConferences.add(conferenceA);
		allConferences.add(conferenceB);
		
		this.allConferences = Collections.unmodifiableList(allConferences);
	}

	public List<Team> getTeams() {
		return allTeams;
	}

	public List<Conference> getConferences() {
		return allConferences;
	}

	public String getName() {
		return "National Hockey League";
	}

}
