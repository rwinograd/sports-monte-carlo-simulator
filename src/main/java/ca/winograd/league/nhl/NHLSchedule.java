package ca.winograd.league.nhl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ca.winograd.league.Team;
import ca.winograd.league.nhl.Game.Result;

public class NHLSchedule {

	// Example: FINAL: ARI (1) - WPG (2)
	private static final Pattern FINAL_SCORE = Pattern.compile("FINAL: \\w{3} \\((\\d*)\\) - \\w{3} \\((\\d*)\\)\\s?(.*)");
	private static final String SOURCE = "http://www.nhl.com/ice/schedulebyseason.htm";
	
	private final Map<Team, List<Game>> teamGames;
	private final List<Game> unplayedGames;
	private final List<Result> playedGames;
	
	public NHLSchedule(List<Game> unplayedGames, List<Result> playedGames, Map<Team, List<Game>> teamGames) {
		this.unplayedGames = Collections.unmodifiableList(unplayedGames);
		this.playedGames = Collections.unmodifiableList(playedGames);
		this.teamGames = Collections.unmodifiableMap(teamGames);
	}

	public static NHLSchedule newInstance(Map<String, Team> teams) throws IOException {
		Map<Team, List<Game>> teamGames = new HashMap<Team, List<Game>>();
		for(Team team : teams.values()) {
			teamGames.put(team, new LinkedList<Game>());
		}
		
		Document document = Jsoup.connect(SOURCE)
			    .header("Accept-Encoding", "gzip, deflate")
			    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0")
			    .maxBodySize(0)
			    .timeout(600000)
			    .get();
		Elements tables = document.body().getElementsByClass("schedTbl");

		List<Game> unplayedGames = new LinkedList<Game>();
		List<Game.Result> playedGames = new LinkedList<Game.Result>();
		for(Element schedule : tables) {
			Element tbody = schedule.child(1);
			for(Element tr : tbody.children()) {
				if(tr.childNodeSize() != 6) continue;
				
				String awayName = getTeamName(tr.child(1));
				String homeName = getTeamName(tr.child(2));
				
				final Game game = new Game(teams.get(homeName), teams.get(awayName));
				teamGames.get(game.getAwayTeam()).add(game);
				teamGames.get(game.getHomeTeam()).add(game);
				
				Matcher matcher = FINAL_SCORE.matcher(getScore(tr.child(4))); 
				if(matcher.matches()) {
					playedGames.add(newGameResult(game, matcher));
				} else {
					unplayedGames.add(game);
				}
			}
		}
		
		return new NHLSchedule(unplayedGames, playedGames, teamGames);
	}

	public List<Game> getUnplayedGames() {
		return unplayedGames;
	}
	
	public List<Game.Result> getPlayedGames() {
		return playedGames;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof NHLSchedule)) return false;
		
		NHLSchedule other = (NHLSchedule) o;
		return (other.unplayedGames.size() == unplayedGames.size());
	}

	private static Game.Result newGameResult(Game game, Matcher matcher) {
		final int awayScore = Integer.parseInt(matcher.group(1));
		final int homeScore = Integer.parseInt(matcher.group(2)); 
		final int periods = periodsFor(matcher.group(3));
		
		return new Game.Result(game, homeScore, awayScore, periods);
	}
	
	private static int periodsFor(String group) {
		if(group.equals("S/O")) return 5;
		if(group.equals("OT")) return 4;
		return 3;
	}

	private static CharSequence getScore(Element child) {
		return child.text();
	}
	
	private static String getTeamName(Element child) {
		return child.getElementsByClass("teamName").first().child(0).html();
	}
}
