package ca.winograd.league.nhl;

import ca.winograd.league.Team;


public class Game {

	public static class Result {
		public final Game game;
		public final int homeScore;
		public final int awayScore;
		public final int periods;

		public Result(Game game, int home, int away, int periods) {
			this.game = game;
			this.homeScore = home;
			this.awayScore = away;
			this.periods = periods;
		}

		public boolean isRegulation() {
			return (periods == 3);
		}
		
		public boolean isOvertime() {
			return (periods == 4);
		}

		public boolean isShootout() {
			return (periods == 5);
		}

		public Team getAwayTeam() {
			return game.getAwayTeam();
		}
		
		public Team getHomeTeam() {
			return game.getHomeTeam();
		}
	}

	private final Team awayTeam;
	private final Team homeTeam;
	
	public Game(Team homeTeam, Team awayTeam) {
		this.homeTeam = homeTeam;
		this.awayTeam = awayTeam;
	}
	
	public Team getAwayTeam() {
		return awayTeam;
	}
	
	public Team getHomeTeam() {
		return homeTeam;
	}
}
