package ca.winograd.league.web.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ca.winograd.league.League;
import ca.winograd.league.Simulator.PlayoffTeamChance;
import ca.winograd.league.Team;
import ca.winograd.league.nhl.NHLSimulator;

import com.codahale.metrics.annotation.Timed;

@Path("/nhl")
@Produces(MediaType.APPLICATION_JSON)
public class NHLResource {

	@GET
	@Timed
	public List<PlayoffTeamChance> sayHello() throws IOException {
		NHLSimulator simulator = new NHLSimulator();
		return simulator.getPlayoffChances();
	}
}
