package ca.winograd.league.web;

import ca.winograd.league.web.resources.NHLResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class LeagueSimulatorApplication extends Application<LeagueSimulatorConfiguration>{
	
	public static void main(String[] args) throws Exception {
	        new LeagueSimulatorApplication().run(args);
    }
	 
	@Override
	public void run(LeagueSimulatorConfiguration configuration, Environment environment)
			throws Exception {
	    environment.jersey().register(new NHLResource());
	    final LeagueSimulatorHealthCheck healthCheck = new LeagueSimulatorHealthCheck();
        environment.healthChecks().register("template", healthCheck);
	}

}
