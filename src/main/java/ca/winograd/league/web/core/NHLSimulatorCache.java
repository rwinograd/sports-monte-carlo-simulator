package ca.winograd.league.web.core;

import ca.winograd.league.nhl.NHLSimulator;
import io.dropwizard.lifecycle.Managed;

public class NHLSimulatorCache implements Managed {

	private final NHLSimulator simulator = new NHLSimulator();
	
	public void start() throws Exception {
		
	}

	public void stop() throws Exception {
		
	}

}
