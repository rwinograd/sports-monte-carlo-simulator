package ca.winograd.league.web;

import com.codahale.metrics.health.HealthCheck;

public class LeagueSimulatorHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}

}
