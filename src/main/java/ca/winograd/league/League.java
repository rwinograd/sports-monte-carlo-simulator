package ca.winograd.league;

import java.util.List;

/**
 * Represents 
 * @author rwinograd
 *
 */
public interface League {
	public String getName();
	public List<Team> getTeams();
}
